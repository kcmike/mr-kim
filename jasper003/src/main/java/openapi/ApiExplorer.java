package openapi;

/* Java 샘플 코드 */


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import com.opencsv.*;

public class ApiExplorer {
	private static InputStream getFromGovernment() throws IOException {
        StringBuilder urlBuilder = new StringBuilder("http://openapi.epost.go.kr/postal/retrieveNewAdressAreaCdService/retrieveNewAdressAreaCdService/getNewAddressListAreaCd"); /*URL*/
        urlBuilder.append("?" + URLEncoder.encode("ServiceKey","UTF-8") + "=XxwZj3Rp47oq16rSB6FMS%2BALhDWcos%2BQwwEHeG3ALejq4mNfoowUFOL9ng1NF7OUwqCfVGty2kxya7Hzf2mf7w%3D%3D"); /*Service Key*/
        urlBuilder.append("&" + URLEncoder.encode("searchSe","UTF-8") + "=" + URLEncoder.encode("dong", "UTF-8")); /*dong : 동(읍/면)명 road :도로명[default] post : 우편번호 */
        urlBuilder.append("&" + URLEncoder.encode("srchwrd","UTF-8") + "=" + URLEncoder.encode("주월동 408-1", "UTF-8")); /*검색어*/
        urlBuilder.append("&" + URLEncoder.encode("countPerPage","UTF-8") + "=" + URLEncoder.encode("10", "UTF-8")); /*페이지당 출력될 개수를 지정*/
        urlBuilder.append("&" + URLEncoder.encode("currentPage","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8")); /*출력될 페이지 번호*/
        URL url = new URL(urlBuilder.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-type", "application/json");
        System.out.println("Response code: " + conn.getResponseCode());
		return conn.getInputStream();
	}
	private static List<Address> parseXml(InputStream inp) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.parse(inp);
		System.out.println("Built XML parser");

		NodeList addresses = document.getElementsByTagName("newAddressListAreaCd");
		List<Address> allAddresses = new ArrayList<>();
		for (int i = 0; i < addresses.getLength(); i++) {
			Element address = (Element)addresses.item(i);
			String current = null, old = null, zip = null;
			NodeList children = address.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Element child = (Element)children.item(j);
				switch (child.getNodeName()) {
				case "zipNo":
					zip = child.getFirstChild().getNodeValue();
					break;
				case "lnmAdres":
					current = child.getFirstChild().getNodeValue();
					break;
				case "rnAdres":
					old = child.getFirstChild().getNodeValue();
					break;
				}
			}
			allAddresses.add(new Address(current, old, zip));
		}
		return allAddresses;
	}
    public static void main(String[] args) throws Exception {
		CSVWriter writer = new CSVWriter(new FileWriter("output.csv"));
		for (Address a : parseXml(getFromGovernment())) {
			writer.writeNext(a.asCsvRow());
		}
		writer.close();
    }
}
