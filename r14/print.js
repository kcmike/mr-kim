$(function() {
    var jsonData;
    // 파일 입력 엘리먼트 가져오기 
    var fileInput = document.getElementById('fileInput');

    // 파일 입력 엘리먼트를 감지하여 변경이 되면 콜백 함수 수행
    fileInput.addEventListener('change', function(e) {
        $("#loading").show();
        handleDrop(e);
    });
    $("#loading").hide();

    function handleDrop(e) {
        //     e.stopPropagation();
        //       e.preventDefault();
        // 파일 객체 가져오기 
        var files = fileInput.files;
        var i, f;
        for (i = 0; i != files.length; ++i) {
            f = files[i];
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function(e) {
                var data = e.target.result;

                // xls  read 처리 
                // 바이너리 모드로 읽었기 떄문에 type 을 바이너리로 설정한다.
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });

                console.log(workbook);
                var sheet_name_list = workbook.SheetNames;
                // 첫번쨰 시트명을 가져옴
                var sheetName = sheet_name_list[0];
                // 시트명의 데이터를 json으로 반환한다.
                json = to_json(workbook, sheetName);
                jsonData = json[sheetName];
                var layout = makeList('');
                
                //var li = makeList('');
                setTimeout(function() {
                    $("#loading").hide();
                    //$('#input_format').hide();
                    //$("#labels").html(li);
                    $("#output").html(layout);
                }, 1000);
                
            };

            // 바이너리로 데이터를 읽어드림 
            reader.readAsBinaryString(f);
        }
    }

    $("#printBtn").on('click', function() {
        window.print();
    });

    //fileInput.addEventListener('drop', handleDrop, false);

    // 시트 데이터를 json 으로 변환시켜줌
    function to_json(workbook, sheetName) {
        var result = {};
        // workbook.SheetNames.forEach(function(sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
        //   });
        return result;
    }

    $("#dear").on('keyup', function(k){
        $(".dear").text($(this).val());
    });
    

    $("#search").on('keyup', function(){
        var search = $(this).val();
        var layout = makeList(search);
        $("#output").html(layout);
        //$("#labels").html(li);
    });

    function makeList(search){
        var rowsPerPage = 14;
        var marginTop = 0; // 맨 위 마진
        var myArray = [];
        // list 배열을 순회하면서 data 정보를 가져옴 
        var i = 0;
        $.each(jsonData, function(k, item) {
            var arr = [];
            for (var prop in item) {
                arr.push(item[prop]);
            }

            if(arr[0].indexOf(search) <= -1){
                return;
            }
            myArray.push(arr);
            //console.log(arr);
            //console.log('-----');
        });
        console.log(myArray);

        var layout = '';
            for (var i = 0; i < myArray.length; i++) {
        
                var num = Math.floor((i%14)/2);

                if(i % rowsPerPage == 0) layout += '<div class="mother">';
                var block = '';
                if (i % 2 == 0) {
                    block = '<div class="block"><div class="left">';
                } else {
                    block = '<div class="right">';
                }
                
                block += '<div>' + (myArray[i][2] ? myArray[i][2] : '&nbsp') + '</div>';
                block += '<div>' + (myArray[i][3] ? myArray[i][3] : '&nbsp') + '</div>';
                block += '<div class="name">' + (myArray[i][0] ? myArray[i][0] : '&nbsp') + '<span class="dear">귀하</span></div>';
                block += '<div class="post">' + (myArray[i][1] ? myArray[i][1] : '&nbsp') + '</div>';
                
                if (i % 2 == 0) {
                    block += '</div>';
                } else {
                    block += '</div></div>';
                }
                
                layout += block;
                if((i + 1) % rowsPerPage == 0) layout += '</div>';
            
            }

        return layout;
    }

});
