package openapi;

/* Java 샘플 코드 */


import java.io.*;
import java.nio.file.Files;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import com.itextpdf.io.font.*;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.font.*;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

public class GeneratePdf extends HttpServlet {
	private static InputStream getFile(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        System.out.println("Response code: " + conn.getResponseCode());
		return conn.getInputStream();
	}
	private static List<Address> parseXml(InputStream inp) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.w3c.dom.Document document = builder.parse(inp);
		System.out.println("Built XML parser");

		NodeList addresses = document.getElementsByTagName("person");
		List<Address> allAddresses = new ArrayList<>();
		for (int i = 0; i < addresses.getLength(); i++) {
			Element address = (Element)addresses.item(i);
			String name = null, a1 = null, a2 = null, zip = null;
			NodeList children = address.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Element child = (Element)children.item(j);
				switch (child.getNodeName()) {
				case "name":
					name = child.getFirstChild().getNodeValue();
					break;
				case "postNumber":
					zip = child.getFirstChild().getNodeValue();
					break;
				case "address01":
					a1 = child.getFirstChild().getNodeValue();
					break;
				case "address02":
					a2 = child.getFirstChild().getNodeValue();
					break;
				}
			}
			allAddresses.add(new Address(name, a1, a2, zip));
		}
		return allAddresses;
	}

	private List<Address> getReportData(URL url) throws Exception {
		return parseXml(getFile(url));
	}

	private static void addCell(Table t, String s) {
		t.addCell(new Cell().add(new Paragraph(s)));
	}
	private byte[] generatePdf(List<Address> addresses) throws IOException {
		PdfFont font = PdfFontFactory.createFont("fonts/malgun.ttf", PdfEncodings.IDENTITY_H);
		File outputFile = File.createTempFile("mrkim", ".pdf");
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(outputFile));
		Document doc = new Document(pdfDoc).setFont(font);
		Table t = new Table(new float[] { 120, 120, 120, 120 });
		for (String s : new String[] { "Name", "Address 1", "Address 2", "Postal code" }) {
			addCell(t, s);
		}
		for (Address a : addresses) {
			addCell(t, a.name);
			addCell(t, a.address1);
			addCell(t, a.address2);
			addCell(t, a.zip);
		}
		doc.add(t);
		doc.close();

		byte[] contents = Files.readAllBytes(outputFile.toPath());

		outputFile.delete();

		return contents;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		URL url = new URL(request.getParameter("url"));
		byte[] pdfData = null;
		try {
			pdfData = generatePdf(getReportData(url));
		} catch (Exception e) {
			throw new ServletException(e);
		}

		response.setContentType("application/pdf");
		response.setContentLength(pdfData.length);
		try (ServletOutputStream out = response.getOutputStream()) {
			out.write(pdfData);
		}
	}
}
