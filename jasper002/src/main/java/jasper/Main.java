package jasper;

import net.sf.jasperreports.engine.*;
import java.io.*;

public class Main {
	public static void main(String[] args) throws JRException {
		JasperExportManager.exportReportToPdfFile(new JasperPrint(), "output.pdf");
	}
}
