package com.kocachic.mrkim;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import java.io.*;
import java.net.*;
import java.util.*;

public class Print {
    private static final String URL = "http://ahhoinn.dothome.co.kr/myData/myMultiline01.txt";

	public static void main(String[] args) {
		try {
			JasperReport report = JasperCompileManager.compileReport(Print.class.getResourceAsStream("/report004.jrxml"));
            System.out.println("Compiled report");
			JRDataSource dataSource = new JRBeanCollectionDataSource(getReportData(getRemoteFile(URL)));
            System.out.println("Created data source from " + URL);
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, null, dataSource);
            System.out.println("Filled report with data source");
			byte[] pdfData = JasperExportManager.exportReportToPdf(jasperPrint);
            System.out.println("Exported to PDF data");
			writeToFile(pdfData);
            System.out.println("Wrote PDF data to disk");
		} catch (JRException|IOException e) {
			System.err.println(e);
		}
	}
	private static void writeToFile(byte[] data) throws IOException {
		File f = new File("test.pdf");
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(data);
		fos.close();
	}
    private static InputStream getRemoteFile(String url) throws IOException {
        URL u = new URL(url);
        HttpURLConnection connection = (HttpURLConnection)u.openConnection();
        return connection.getInputStream();
    }
    private static List<Map<String, Object>> getReportData(InputStream s) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(s));
        String line = r.readLine();
        List<Map<String, Object>> ret = new ArrayList<>();
        while ((line = r.readLine()) != null) {
            String[] fields = line.split(",");
            Map<String, Object> m = new HashMap<>();
            m.put("name", fields[0]);
            m.put("postNumber", fields[1]);
            m.put("address01", fields[2]);
            m.put("address02", fields[3]);
            ret.add(m);
        }
        return ret;
    }
}
