package com.kocachic.mrkim;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import javax.servlet.*;
import javax.servlet.http.*;
import java.net.*;
import java.io.*;
import java.util.*;

public class Test1 extends HttpServlet {
	private final static String URL = "http://ahhoinn.dothome.co.kr/myData/myMultiline01.txt";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		byte[] pdfData = null;
		try {
			JasperReport report = JasperCompileManager.compileReport(getClass().getResourceAsStream("/report004.jrxml"));
			JRDataSource dataSource = new JRBeanCollectionDataSource(getReportData(getRemoteFile(URL)));
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, null, dataSource);
			pdfData = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			throw new ServletException(e);
		}
		
		response.setContentType("application/pdf");
		response.setContentLength(pdfData.length);
		try (ServletOutputStream out = response.getOutputStream()) {
			out.write(pdfData);
		}
	}

	private InputStream getRemoteFile(String url) throws IOException {
		URL u = new URL(url);
		HttpURLConnection connection = (HttpURLConnection)u.openConnection();
		return connection.getInputStream();
	}

	private List<Map<String, Object>> getReportData(InputStream in) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        String line = r.readLine();
		List<Map<String, Object>> data = new ArrayList<>();
        while ((line = r.readLine()) != null) {
            String[] fields = line.split(",");
            Map<String, Object> item = new HashMap<>();
            item.put("name", fields[0]);
            item.put("postNumber", fields[1]);
            item.put("address01", fields[2]);
            item.put("address02", fields[3]);
            data.add(item);
		}
		return data;
	}
}
