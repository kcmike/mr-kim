# Installing

1. Install Ubuntu
2. `sudo apt install tomcat8 tomcat8-admin gradle git`
3. `git clone https://gitlab.com/kcmike/mr-kim.git`
4. `sudo vi /etc/tomcat8/tomcat-users.xml` to add the following lines:
    
    ```xml
    <role rolename="manager-gui"/>
    <user username="admin" password="xxxx" roles="manager-gui"/>
    ```
    
5. `sudo systemctl restart tomcat8`
6. `cd mr-kim/j`
7. `gradle build`

# Running

## Running jar file

```bash
java -jar build/libs/j-1.0.jar
```

A new file called `test.pdf` will be produced in the current directory.

## Running web application

Go to [the Tomcat admin page](http://localhost:8080/manager/html) logging in as user `admin` and password `xxxx` as configured before.

Scroll down to "WAR file to deploy" and click on the "Browse..." button. Select `mr-kim/j/build/libs/j.war` and click "Deploy"

Then, [try to load the servlet](http://localhost:8080/j/test1)
