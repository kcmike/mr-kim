package openapi;

/* Java 샘플 코드 */


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import org.json.*;
import java.sql.*;

public class GetFromSqlite {
	private static List<Address> retrieveFromDatabase() throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:sqlite:addresses.sqlite");
		Statement stmt = conn.createStatement();
		List<Address> r = new ArrayList<>();
		ResultSet rs = stmt.executeQuery("SELECT * FROM addresses");
		while (rs.next()) {
			Address a = new Address(rs.getString("currentAddress"),
				rs.getString("oldAddress"), rs.getString("postalCode"));
			r.add(a);
		}
		conn.close();
		return r;
	}
    public static void main(String[] args) throws Exception {
		List<Address> addresses = retrieveFromDatabase();
		System.out.println(addresses);
		JSONObject root = new JSONObject();
		JSONArray array = new JSONArray();
		for (Address a : addresses) {
			JSONObject o = new JSONObject();
			o.put("currentAddress", a.currentAddress);
			o.put("oldAddress", a.oldAddress);
			o.put("postalCode", a.zip);
			array.put(o);
		}
		root.put("addresses", array);
		try (FileWriter f = new FileWriter("output.json")) {
			root.write(f);
		}
    }
}
