package openapi;

/* Java 샘플 코드 */


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import com.opencsv.*;
import java.sql.*;

public class GetFromSqlite {
	private static List<Address> retrieveFromDatabase() throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:sqlite:addresses.sqlite");
		Statement stmt = conn.createStatement();
		List<Address> r = new ArrayList<>();
		ResultSet rs = stmt.executeQuery("SELECT * FROM addresses");
		while (rs.next()) {
			Address a = new Address(rs.getString("currentAddress"),
				rs.getString("oldAddress"), rs.getString("postalCode"));
			r.add(a);
		}
		conn.close();
		return r;
	}
    public static void main(String[] args) throws Exception {
		List<Address> addresses = retrieveFromDatabase();
		System.out.println(addresses);
		CSVWriter writer = new CSVWriter(new FileWriter("output.csv"));
		for (Address a : addresses) {
			String[] fields = a.asCsvRow();
			writer.writeNext(fields);
		}
		writer.close();
    }
}
