package openapi;

class Address {
	public String name, address1, address2, zip;
	public Address(String name, String address1, String address2, String zip) {
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.zip = zip;
	}

	public String toString() {
		return "(" + name + "," + address1 + ", " + address2 + ", " + zip + ")";
	}
}
