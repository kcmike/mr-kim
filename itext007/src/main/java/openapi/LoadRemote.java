package openapi;

/* Java 샘플 코드 */

import java.io.*;
import java.nio.file.Files;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import com.itextpdf.io.font.*;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.kernel.font.*;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

abstract public class LoadRemote extends HttpServlet {
	private static InputStream getFile(URL url) throws IOException {
		if (url == null)	return null;
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        System.out.println("Response code: " + conn.getResponseCode());
		return conn.getInputStream();
	}
	protected abstract List<Address> parse(InputStream inp) throws Exception;
	protected List<Address> parse(InputStream inp, HttpServletRequest req) throws Exception {
		return parse(inp);
	}

	private List<Address> getReportData(URL url, HttpServletRequest req) throws Exception {
		return parse(getFile(url), req);
	}

	private static void addCell(Table t, String s) {
		t.addCell(new Cell().add(new Paragraph(s)));
	}
	private byte[] generatePdf(List<Address> addresses) throws IOException {
		PdfFont font = PdfFontFactory.createFont("fonts/malgun.ttf", PdfEncodings.IDENTITY_H);
		File outputFile = File.createTempFile("mrkim", ".pdf");
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(outputFile));
		Document doc = new Document(pdfDoc).setFont(font);
		Table t = new Table(new float[] { 120, 120, 120, 120 });
		for (String s : new String[] { "Name", "Address 1", "Address 2", "Postal code" }) {
			addCell(t, s);
		}
		for (Address a : addresses) {
			addCell(t, a.name);
			addCell(t, a.address1);
			addCell(t, a.address2);
			addCell(t, a.zip);
		}
		doc.add(t);
		doc.close();

		byte[] contents = Files.readAllBytes(outputFile.toPath());

		outputFile.delete();

		return contents;
	}

	protected void generatePDF(List<Address> data, HttpServletResponse response) throws IOException {
		byte[] pdfData = generatePdf(data);
		response.setContentType("application/pdf");
		response.setContentLength(pdfData.length);
		try (ServletOutputStream out = response.getOutputStream()) {
			out.write(pdfData);
		}
	}

	protected void afterLoading(List<Address> d, HttpServletResponse r) throws IOException {
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			List<Address> data = getReportData(null, request);
			afterLoading(data, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
