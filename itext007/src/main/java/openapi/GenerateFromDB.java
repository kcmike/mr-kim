package openapi;

import java.util.*;
import javax.servlet.http.HttpServletResponse;

public class GenerateFromDB extends LoadRemote {
	protected List<Address> parse(java.io.InputStream unused) throws Exception {
		Database db = new Database();
		return db.getAllAddresses();
	}
	@Override
	protected void afterLoading(List<Address> d, HttpServletResponse r) throws java.io.IOException {
		generatePDF(d, r);
	}
}
