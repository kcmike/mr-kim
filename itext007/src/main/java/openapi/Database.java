package openapi;

import java.sql.*;
import java.util.*;

public class Database {
	private Connection conn;
	public Database() throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		this.conn = DriverManager.getConnection("jdbc:sqlite:/tmp/addresses2.sqlite");
	}
	private List<Address> getAddresses(ResultSet rs) throws SQLException {
		ArrayList<Address> a = new ArrayList<>();
		while (rs.next()) {
			String n = rs.getString("name");
			String z = rs.getString("postCode");
			String a1 = rs.getString("address01");
			String a2 = rs.getString("address02");
			Address aa = new Address(n, z, a1, a2);
			a.add(aa);
		}
		return a;
	}
	public List<Address> getAllAddresses() throws SQLException {
		try (Statement stmt = conn.createStatement()) {
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM address2")) {
				return getAddresses(rs);
			}
		}
	}
}
