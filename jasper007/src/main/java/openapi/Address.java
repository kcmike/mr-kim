package openapi;

class Address {
	public String currentAddress, oldAddress, zip;
	public Address(String current, String old, String zip) {
		this.currentAddress = current;
		this.oldAddress = old;
		this.zip = zip;
	}

	public String toString() {
		return "(" + currentAddress + ", " + oldAddress + ", " + zip + ")";
	}

	public String[] asCsvRow() {
		return new String[] { currentAddress, oldAddress, zip };
	}
}
