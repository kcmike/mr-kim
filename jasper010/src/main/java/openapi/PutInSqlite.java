package openapi;

/* Java 샘플 코드 */


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import org.json.*;
import java.sql.*;

public class PutInSqlite {
	private static void insertIntoDatabase(List<Address> addresses) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:sqlite:addresses.sqlite");
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE TABLE IF NOT EXISTS addresses " +
			"(currentAddress VARCHAR(256), oldAddress VARCHAR(256), postalCode VARCHAR(16))");
		for (Address a : addresses) {
			PreparedStatement s = conn.prepareStatement("INSERT INTO addresses " +
				"(currentAddress, oldAddress, postalCode) VALUES (?,?,?)");
			s.setString(1, a.currentAddress);
			s.setString(2, a.oldAddress);
			s.setString(3, a.zip);
			s.executeUpdate();
		}
		conn.close();
	}
    public static void main(String[] args) throws Exception {
		List<Address> addresses = new ArrayList<>();
		InputStream inp = new FileInputStream("input.json");
		String contents = new java.util.Scanner(inp).useDelimiter("\\A").next();
        JSONArray root = new JSONObject(contents).getJSONArray("addresses");
		for (Object a_ : root) {
			JSONObject a = (JSONObject)a_;
			addresses.add(new Address(a.getString("currentAddress"),
				a.getString("oldAddress"), a.getString("postalCode")));
		}
		inp.close();
		insertIntoDatabase(addresses);
    }
}
